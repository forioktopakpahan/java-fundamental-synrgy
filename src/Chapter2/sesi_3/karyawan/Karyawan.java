package Chapter2.sesi_3.karyawan;

import java.time.LocalDate;

public class Karyawan {
    //field
    private Integer id;
    private String nama;
    private LocalDate DoB;
    private String alamat;
    private Boolean status;

    // constructor
    public Karyawan(Integer id, String nama, LocalDate DoB, String alamat, Boolean status){
        this.id = id;
        this.nama = nama;
        this.DoB = DoB;
        this.alamat = alamat;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public LocalDate getDoB() {
        return DoB;
    }

    public void setDoB(LocalDate doB) {
        DoB = doB;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getGaji(){
        int upah = 200_000;
        return (2023 - DoB.getYear()) * upah;
    }
}
