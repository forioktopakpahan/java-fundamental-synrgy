package Chapter2.sesi_3;

import java.util.*;

public class materi {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("kursi");
        list.add("meja");
        list.add("bangku");
        list.add("table");
        System.out.println("isi dari list");
        for(String l : list){
            System.out.println(l);
        }
        System.out.println("==============");
        for(int i = 0; i < list.size(); i++){
            System.out.println(list.get(i));
        }


        Set<String> names = new HashSet<>();
        names.add("TOM");
        names.add("THUMB");
        names.add("DOM");
        names.add("DOM");
        System.out.println("==============");
        for(String s: names){
            System.out.println(s);
        }
        System.out.println("==============");
        Iterator<String> iterator = names.iterator();
        while(iterator.hasNext()){
            String name = iterator.next();
            System.out.println(name);
        }


        System.out.println("======MAP=====");

        Map<String, String> mapCountryCodes = new HashMap<>();
        mapCountryCodes.put("1", "USA");
        mapCountryCodes.put("2", "INDONESIA");
        mapCountryCodes.put("3", "THAI");
        mapCountryCodes.put("10", "JAPAN");
        mapCountryCodes.put("11", "SWISS");

        Set<String> setCodes = mapCountryCodes.keySet();
        System.out.println(setCodes);
//        for(String setCode : setCodes){
//            System.out.println(setCode + " => " + mapCountryCodes.get(setCode));
//        }
        Iterator<String> iterators = setCodes.iterator();
        while (iterators.hasNext()){
            String code = iterators.next();
            String country = mapCountryCodes.get(code);

            System.out.println(code + " => " + country);
        }

        System.out.println("======MAP=====");
        Map<Integer, String> mapHttpError = new HashMap<>();
        mapHttpError.put(200, "OK");
        mapHttpError.put(303, "See Other");
        mapHttpError.put(404, "Not found");
        mapHttpError.put(500, "Internal Server Error");

        for(Integer i : mapHttpError.keySet()){
            System.out.println(i + " => " + mapHttpError.get(i));
        }


    }
}
/*
! 2 Interface paling atas yaitu ITERABLE DAN MAP
* iterable adalah kumpulan suatu elemen yang punya sequence
* map adalah kumpulan suatu elemen yang dipetakan pakai key dan value

! 3 collection yang paling sering digunakan
* List => adlaha jenis collection yang mewariskan sifat dari interface iterable yang pnya urutan (indexing)
* Map
* Set

todo => List
? ArrayList => list yang mirip array, tapi sizenya bisa bertambah menyesuaikan element-nya (indexing)
* Vector
* LinkedList

todo => set
* turunan dari collection yang nggak kenal sama yang namanya index. dikarenakan nggak kenal, setiap element dari sebuah set harus unik atau nggak boleh duplikated
* set tidak punya rutuan dan bisa ngacak. class turunannya adalah

* HashSet => set yg nggak boleh ada duplikasi dan nggak punya urutan
* LinkedHashSet => merupakan set dengan urutan berdasarkan urutan penambahan element
* TreeSet =>  merupakan set yang bisa melakukan sorting berdasarkan valuenya

todo => map
* bentuk struktur data yang berpasangan antara key dan value. Key dan value yang dimaksud merupakan sebuah object
* key ini harus bersifat unik karena implementasinya pakai set

* HashMap => map turunan yang nggak punya urutan dan memperbolehkan null
* LinkedHashMap => turunan dari sebuah HashMap yang punya urutan berdasarkan kapan sebuah element ditambahkan
* TreeMap => map yang nggak memperbolehkan adanya null karena didalam TreeMap, key atau value bisa dilakukan sorting

* */