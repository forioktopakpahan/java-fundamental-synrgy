package Chapter2.sesi_1;

public class Manusia {
    String nama;

    Manusia(String nama){
        this.nama = nama;
    }

    void berjalan(){
        System.out.println("nama = " + this.nama);
    }
}