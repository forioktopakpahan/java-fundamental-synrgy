package Chapter2.sesi_1.karyawan;

import java.util.Date;

public class KaryawanKontrak extends Karyawan{
    final int durasikontrak;

    public KaryawanKontrak(int id, String nama, Date dob, String alamat, boolean status, int durasikontrak){
        super(id, nama, dob, alamat, status);
        this.durasikontrak = durasikontrak;
    }

    public void cetakInfo() {
        super.cetakInfo();
        System.out.println("Durasi Kontrak: " + durasikontrak + " bulan");
    }
}
