package Chapter2.sesi_1.karyawan;

import java.util.Date;

public class KaryawanTetap extends Karyawan{
    private double gaji;

    public KaryawanTetap(int id, String nama, Date dob, String alamat, boolean status, double gaji){
        super(id, nama, dob, alamat, status);
        this.gaji=gaji;
    }

    @Override
    public void cetakInfo() {
        super.cetakInfo();
        System.out.println("Gaji: " + gaji);
    }
}
