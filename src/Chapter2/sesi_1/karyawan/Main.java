package Chapter2.sesi_1.karyawan;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {
    static Scanner inputpilihan = new Scanner(System.in);
    public static void main(String[] args) {
        ArrayList<Karyawan> daftarKaryawan = new ArrayList<>();

        daftarKaryawan.add(new KaryawanTetap(1,"fori", new Date(), "pangaribuan", true, 5000));
        daftarKaryawan.add(new KaryawanKontrak(2, "okto", new Date(), "pangaribuan", false, 2000));

        for(Karyawan karyawan : daftarKaryawan){
            karyawan.cetakInfo();
            System.out.println("===============");
        }
    }


}
