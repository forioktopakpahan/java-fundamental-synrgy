package Chapter2.sesi_1.karyawan;

import sun.text.bidi.BidiLine;

import java.util.Date;

public class Karyawan {
    final int id;
    final String nama;
    final Date dob;
    final String alamat;
    public boolean status;
    public Karyawan(int id, String nama, Date dob, String alamat, boolean status){
        this.id = id;
        this.nama = nama;
        this.dob = dob;
        this.alamat = alamat;
        this.status = status;
    }

    //getter
    public boolean isAktif(){
        return status;
    }

    //setter
    public void setAktif(boolean status){
        this.status = status;
    }

    public void cetakInfo(){
        System.out.println("ID: " + id);
        System.out.println("Nama: " + nama);
        System.out.println("Tanggal Lahir: " + dob);
        System.out.println("Alamat: " + alamat);
        System.out.println("Status: " + (status ? "aktif":"tidak aktif"));
    }
}
