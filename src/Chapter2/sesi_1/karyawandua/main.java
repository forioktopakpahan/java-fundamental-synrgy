package Chapter2.sesi_1.karyawandua;
import java.util.ArrayList;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        ArrayList<Karyawan> daftarKaryawan = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        int pilihan;
        do {
            System.out.println("Menu:");
            System.out.println("1. tambah karyawan tetap");
            System.out.println("2. tambah karyawan kontrak");
            System.out.println("3. tampilkan data karyawan");
            System.out.println("0. keluar");
            System.out.print("Pilihan anda => ");
            pilihan = scanner.nextInt();
            scanner.nextLine();

            switch (pilihan){
                case 1:
                    System.out.print("masukkan nama karyawan: ");
                    String namaTetap = scanner.nextLine();
                    System.out.print("masukkan jabatan: ");
                    String jabatanTetap = scanner.nextLine();
                    System.out.print("masukkan gaji bulanan: Rp.");
                    double gajiTetap = scanner.nextDouble();
                    daftarKaryawan.add(new KaryawanTetap(namaTetap, jabatanTetap, gajiTetap));
                    break;
                case 2:
                    System.out.print("masukkan nama karyawan: ");
                    String namaKontrak = scanner.nextLine();
                    System.out.print("masukkan jabatan: ");
                    String jabatanKontrak = scanner.nextLine();
                    System.out.print("masukkan gaji bulanan: Rp.");
                    double gajiKontrak = scanner.nextDouble();
                    System.out.print("masukkan durasi kontrak: ");
                    int durasiKontrak = scanner.nextInt();
                    daftarKaryawan.add(new KaryawanKontrak(namaKontrak, jabatanKontrak, gajiKontrak, durasiKontrak));
                    break;
                case 3:
                    System.out.println("data karyawan");
                    for(Karyawan karyawan: daftarKaryawan){
                        karyawan.tampilkanInfo();
                        System.out.println("-----------------");
                    }
                    break;
                case 0:
                    break;
            }
        }while (pilihan != 0);
    }
}
