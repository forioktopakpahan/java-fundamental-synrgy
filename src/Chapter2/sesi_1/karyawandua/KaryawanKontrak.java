package Chapter2.sesi_1.karyawandua;

class KaryawanKontrak extends KaryawanTetap{
    private int lamaKerja;
    public KaryawanKontrak(String nama, String jabatan, double gaji, int lamaKerja){
        super(nama, jabatan, gaji);
        this.lamaKerja = lamaKerja;
    }
    public void tampilkanInfo(){
        super.tampilkanInfo();
        System.out.println("Durasi Kontrak : " + lamaKerja + " tahun");
    }
}
