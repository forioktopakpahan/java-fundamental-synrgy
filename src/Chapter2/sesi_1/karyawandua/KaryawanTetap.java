package Chapter2.sesi_1.karyawandua;

class KaryawanTetap extends Karyawan{
    private double gaji;

    public KaryawanTetap(String nama, String jabatan, double gaji){
        super(nama, jabatan);
        this.gaji = gaji;
    }
    public void tampilkanInfo(){
        super.tampilkanInfo();
        System.out.println("Gaji Bulanan: Rp." + this.gaji);
    }
}
