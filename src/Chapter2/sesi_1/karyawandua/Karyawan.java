package Chapter2.sesi_1.karyawandua;

class Karyawan {
    // ? fields
    private String nama;
    private String jabatan;

    // ? constructor
    public Karyawan(String nama, String jabatan){
        this.nama = nama;
        this.jabatan = jabatan;
    }

    public void tampilkanInfo(){
        System.out.println("Nama: " + this.nama);
        System.out.println("Jabatan: " + this.jabatan);
    }
}
