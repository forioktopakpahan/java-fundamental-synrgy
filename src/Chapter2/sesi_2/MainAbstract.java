package Chapter2.sesi_2;

public class MainAbstract {
    public static void main(String[] args) {
        Employee employee = new Employee();
        employee.setName("nano");
        System.out.println(employee.getName());
        employee.changeName("nanotest");
        System.out.println(employee.getName());
        employee.makePerson();
        employee.work();

    }
}
