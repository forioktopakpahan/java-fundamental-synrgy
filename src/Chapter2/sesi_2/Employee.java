package Chapter2.sesi_2;

public class Employee extends Person{
    private String EmployeeId;
    private static final String TEST_STATIC_FINAL = "INDONESIA";
    public Employee(){
        super();
    }
    @Override
    public void work(){
        System.out.println("working in " + TEST_STATIC_FINAL);
    }
}
/*
* method abstract harus di implement pada subclass. mau tidak mau.
* */