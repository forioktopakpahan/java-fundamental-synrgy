package Chapter2.sesi_2;

import com.sun.media.jfxmediaimpl.HostUtils;

public class FootballMatch implements Football{
    private String name;
    private int points;
    private String awayName;

    private int awayPoints;
    public FootballMatch(String name, int points){
        this.name = name;
        this.points = points;
    }
    public void setHomeTeam(String name){
        this.name = name;
    }

    public void setVisitingTeam(String name){
        this.awayName = name;
    }

    public void homeTeamScored(int points){
        System.out.println(this.name + " score is " + points);
    }

    public void visitingTeamScored(int points){
        this.awayPoints = points;
        System.out.println(this.awayName + " score is " + points);
    }

    public void gameResult(){
        System.out.println(name + " score " + points);
        System.out.println(awayName + " score " + awayPoints);
        if(points > awayPoints){
            System.out.println("The winner is " + name);
        }else if(points == awayPoints){
            System.out.println(name + " draw againts " + awayName);
        }else {
            System.out.println("The winner is " + awayName);
        }
    }
}
