package Chapter2.sesi_2;

import jdk.nashorn.internal.objects.annotations.Getter;

public abstract class Person {
    private String name;
    private String gender;
    public Person(){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    protected void changeName(String newName){
        this.name = newName;
    }

    public abstract void work();

    public void makePerson(){
        System.out.println("dinamic person");
    }
}
/*
? Abstraction
* abstraction dilakukan untuk membuat generalisasi dari suatu object, hal ini digunakan untuk mempermudah struktur kode yang kompleks dan mengurangi penulisan code yang berulang-ulang.
* analoginya seperti ini, misalkan ada tugas untuk membuat report. tetapi gk ada penjelasan tentang gimana report itu bisa dibuat, apakah dibuat dengan menggunakan computer, hp, atau tulis tangan. seperti itu kira kira
* abstract class diperbolehkan punya sebuah field dan contructor
* abstraction juga bisa dibuat menggunakan "interface"

* method abstract tidak memiliki body dan harus diimplementasikan pada subclass
* */
