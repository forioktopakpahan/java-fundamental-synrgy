package Chapter2.sesi_2;

public class Outer {
    public Outer(){}

    public static class StatNestedDemo{
        public void myMethod(){
            System.out.println("this is static class");
        }
    }

    class NonStatNestedDemo{
        public void myMethod(){
            System.out.println("This is not a static class");
        }
    }

    public static void main(String[] args) {
        // Inner class bisa langsung digunakaan tanpa instantiate Outer class
        Outer.StatNestedDemo nested = new Outer.StatNestedDemo();
        nested.myMethod();

        // Outer class harus di instantiate
        Outer outer = new Outer();
        Outer.NonStatNestedDemo nonStatNested = outer.new NonStatNestedDemo();
        nonStatNested.myMethod();
    }
}
/*
? Static
* Penggunaan keyword static cuma berlaku pada inner class aja
* penggunaan static keyword pada inner class akan membuat proses instance inner class menjadi bisa dilakukan tanpa harus meng-instance outerclassnya terlebih dahulu
? final
* penggunaan keyword final pada class menyebabkan suatu class nggak bisa memiliki subclass
* artinya class lain nggak bisa meng-extend class yang punya keyword final
* sebuah abstract class jangan dibuat menjadi final class, soalnya abstract class butuh subclass supaya bisa di-instance
* final method memfinalisasi implementasi dari method, sehingga nggak bisa di-override
* final pada field membuat value tidak bisa dimodifikasi lagi
? Static pada field
* menyebabkan dua object yang berbeda tapi berasal dari class yang sama, bisa akses value dari field antara satu dengan lainnya.
* kalau value dari static field di object a berubah, maka value dari static field di object lain juga ikut berubah.
? static final
* penggunaan static final pada suatu field bakal membuat field tersebut menjadi constant
* constant adalah nilai dari suatu field yang bersifat tetap dan nggak bisa dimofikasi.
* */