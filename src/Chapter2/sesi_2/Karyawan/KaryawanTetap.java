package Chapter2.sesi_2.Karyawan;

public class KaryawanTetap extends Karyawan{
    public KaryawanTetap(String nama, int id){
        super(nama, id);
    }



    @Override
    int hitungGaji(int gaji) {
        int harian = 100_000;
        return gaji*harian;
    }

    @Override
    String jenisKaryawan() {
        return "tetap";
    }


}
