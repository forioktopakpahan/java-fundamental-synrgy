package Chapter2.sesi_2.Karyawan;

import java.io.File;
import java.util.Scanner;

public class KaryawanApp {
    public static void main(String[] args) {
        ManajerKaryawan manajerKaryawan = new ManajerKaryawan();

        KaryawanTetap karyawanTetap = new KaryawanTetap("Fori Okto", 2);
        KaryawanMagang karyawanMagang = new KaryawanMagang("pakpahan", 1);
        KaryawanKontrak karyawanKontrak = new KaryawanKontrak("sidabutar", 12);

        manajerKaryawan.tambahKaryawan(karyawanTetap);
        manajerKaryawan.tambahKaryawan(karyawanMagang);
        manajerKaryawan.tambahKaryawan(karyawanKontrak);

        Karyawan karyawanCari = manajerKaryawan.cariKaryawan(13);
        if(karyawanCari != null){
            System.out.println("Karyawan ditemukan: " + karyawanCari.getName());
            System.out.println("Gaji: " + karyawanCari.hitungGaji(12));
        }else{
            System.out.println("Data tidak ditemukan");
        }

        manajerKaryawan.hapusKaryawan(1);
        System.out.println("daftar karyawan");

        for(Karyawan karyawan : manajerKaryawan.getDaftarKaryawan()){
            System.out.println(karyawan.getName() + karyawan.getId());
        }
    }
}
