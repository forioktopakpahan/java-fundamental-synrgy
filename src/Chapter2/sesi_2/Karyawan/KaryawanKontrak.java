package Chapter2.sesi_2.Karyawan;


public class KaryawanKontrak extends Karyawan{
    public KaryawanKontrak(String name, int id) {
        super(name, id);
    }
    @Override
    int hitungGaji(int gaji){
        int harian = 70_000;
        return harian * gaji;
    }

    @Override
    String jenisKaryawan(){
        return "kontrak";
    }

}