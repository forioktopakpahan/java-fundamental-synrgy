package Chapter2.sesi_2.Karyawan;

import Chapter2.sesi_1.karyawan.KaryawanTetap;

public abstract class Karyawan {
    //? field
    protected String name;
    protected int id;

    //? constructor
    public Karyawan(String name, int id){
        this.name = name;
        this.id = id;
    }

    //? getter
    public String getName(){
        return name;
    }
    public int getId(){
        return id;
    }

    //? abstract method
    abstract int hitungGaji(int gaji);
    abstract String jenisKaryawan();

}

// abstract class have to declare in subclass
