package Chapter2.sesi_2.Karyawan;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class MainKaryawanApp {
    static ArrayList<Karyawan> listKaryawan = new ArrayList<>();
    public static void main(String[] args) {
        InsertData();
        MainMenu();
    }

    //! INSERT DATA FUNCTION
    public static void InsertData(){
        try{
            File myFile = new File("src/Chapter2/sesi_2/Karyawan/dataKaryawan.csv");
            Scanner sc = new Scanner(myFile);
            int flag = 0;
            while (sc.hasNextLine()){
                String[] data = sc.nextLine().split(",");
                System.out.println(Arrays.toString(data));
                if(flag == 0){
                    flag = 1;
                    continue;
                }
                int id = Integer.parseInt(data[0]);
                String nama = data[1];
                String jenisKaryawan = data[2].toLowerCase();
                switch (jenisKaryawan){
                    case "tetap":
                        listKaryawan.add(new KaryawanTetap(nama, id));
                        break;
                    case "magang":
                        listKaryawan.add(new KaryawanMagang(nama, id));
                        break;
                    case "kontrak":
                        listKaryawan.add(new KaryawanKontrak(nama, id));
                        break;
                }
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    // ! MAIN MENU FUNCTION
    public static void MainMenu(){
        System.out.println("============================");
        System.out.println("APLIKASI MANAJEMEN KARYAWAN");
        System.out.println("============================");
        System.out.println("1. ADD");
        System.out.println("2. DELETE");
        System.out.println("3. FIND");
        System.out.println("4. EXIT");

        int input;
        while (true){
            input = input();
            if(input >= 1 && input <=4){
                break;
            }else{
                System.out.println("option not available");
            }
        }

        switch (input){
            case 1:
                add();
                break;
            case 2:
                delete();
                break;
            case 3:
                find();
                break;
        }
        writeFile();
    }

    //! INPUT FUNCTION
    public static int input(){
        int inputChoice;
        while (true){
            try{
                System.out.print("=> ");
                Scanner input = new Scanner(System.in);
                inputChoice = input.nextInt();
                break;
            }catch (Exception e){
                System.out.println("invalid choice");
            }
        }
        return inputChoice;
    }

    //! ADD FUNCTION
    public static void add(){
        System.out.print("masukkan nama : ");
        Scanner userInput = new Scanner(System.in);
        String nama = userInput.nextLine();

        System.out.print("Masukkan jenis karyawan (tetap/magang/kontrak) : ");
        Scanner userInput2 = new Scanner(System.in);
        String jenisKaryawan = userInput2.nextLine().toLowerCase();

        int id = listKaryawan.get(listKaryawan.size() - 1).getId() + 1;
        System.out.println(id);
        switch (jenisKaryawan){
            case ("tetap"):
                listKaryawan.add(new KaryawanTetap(nama, id));
                System.out.println("input berhasil");
                break;
            case ("magang"):
                listKaryawan.add(new KaryawanMagang(nama, id));
                System.out.println("input berhasil");
                break;
            case ("kontrak"):
                listKaryawan.add(new KaryawanKontrak(nama, id));
                System.out.println("input berhasil");
                break;
            default:
                System.out.println("invalid jenis karyawan");
            }
        pressAnyKey();
        MainMenu();
    }

    //! DELETE FUNCTION
    public static void delete(){
        allList();
        System.out.print("silahkan pilih id : ");
        int id = input();
        boolean result = listKaryawan.removeIf(karyawan -> karyawan.getId() == id);
        if(result){
            System.out.println("karyawan berhasil dihapus");
        }else{
            System.out.println("karyawan gagal dihapus");
        }
        allList();
        pressAnyKey();
        MainMenu();
    }

    //! FIND FUNCTION
    public static void find(){
        allList();
        System.out.println("silahkan pilih id : ");
        int id = input();
        int flag = 0;
        Karyawan targetedKaryawan;
        for(Karyawan karyawan : listKaryawan){
            if(karyawan.getId() == id){
                System.out.println("=======================");
                targetedKaryawan = karyawan;
                System.out.println("nama           : " + targetedKaryawan.getName());
                System.out.println("id             : " + targetedKaryawan.getId());
                System.out.println("jenis karyawan : " + targetedKaryawan.jenisKaryawan());
                System.out.println("gaji           : " + targetedKaryawan.hitungGaji(10));
                System.out.println("=======================");
                flag = 1;
            }
        }
        if(flag == 0){
            System.out.println("Karyawan tidak ditemukan");
        }
        pressAnyKey();
        MainMenu();
    }

    //! WRITEFILE FUNCTION
    public static void writeFile(){
        try{
            PrintWriter printWriter = new PrintWriter("src/Chapter2/sesi_2/Karyawan/dataKaryawan.csv");
            printWriter.println("id, nama, jabatan");
            for(Karyawan karyawan : listKaryawan){
                printWriter.printf("%d,%s,%s", karyawan.getId(), karyawan.getName(), karyawan.jenisKaryawan().toLowerCase());
                printWriter.println();
            }
            printWriter.close();
        }catch (Exception e){
            e.getStackTrace();
        }

    }
    //! ALLLIST FUNCTION
    public static void allList(){
        System.out.println("======================");
        System.out.printf("%-15s | %-3s", "NAMA", "ID");
        System.out.println();
        System.out.println("======================");
        for(Karyawan karyawan : listKaryawan){
            System.out.printf("%-15s | %-3d", karyawan.getName(), karyawan.getId());
            System.out.println();
        }
    }

    //! PRESSANYKEY FUNCTION
    private static void pressAnyKey() {
        System.out.println("press any key to continue ...");
        Scanner enter = new Scanner(System.in);
        enter.nextLine();
    }
}
