package Chapter2.sesi_2.Karyawan;

import java.util.ArrayList;
import java.util.List;

public class ManajerKaryawan {
    private List<Karyawan> daftarKaryawan = new ArrayList<>();

    public void tambahKaryawan(Karyawan karyawan){
        daftarKaryawan.add(karyawan);
    }

    public void hapusKaryawan(int id){
        daftarKaryawan.removeIf(karyawan -> karyawan.getId() == id);
    }

    public Karyawan cariKaryawan(int id){
        for(Karyawan karyawan : daftarKaryawan){
            if(karyawan.getId() == id){
                return karyawan;
            }
        }
        return null;
    }

    public List<Karyawan> getDaftarKaryawan(){
        return daftarKaryawan;
    }
}
