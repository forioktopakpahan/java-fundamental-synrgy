package Chapter2.sesi_2.Karyawan;

public class KaryawanMagang extends Karyawan{

    // constructor from abstract class have to declare
    public KaryawanMagang(String name, int id) {
        super(name, id);
    }

    @Override
    int hitungGaji(int gaji){
        int harian = 50_000;
        return gaji*harian;
    }
    @Override
    String jenisKaryawan(){
        return "magang";
    }
}
