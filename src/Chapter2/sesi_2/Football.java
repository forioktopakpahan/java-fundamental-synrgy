package Chapter2.sesi_2;

public interface Football extends Sports{
    void homeTeamScored(int points);
    void visitingTeamScored(int points);
}
