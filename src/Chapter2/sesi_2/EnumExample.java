package Chapter2.sesi_2;

public class EnumExample {
    enum Season{
        WINTER(5), SPRING(10), SUMMER(12), FALL(20), DRINK(123,"GASTOV");

        private int value;
        private String valueString;

        private Season(int value){
            this.value = value;
        }
        private Season(int value, String valueString){
            this.value = value;
            this.valueString = valueString;
        }
    }


    public static void main(String[] args) {
        for(Season s : Season.values()){
            System.out.println(s + " " + s.value + s.valueString);
        }
    }
}
