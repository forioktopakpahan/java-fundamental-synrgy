package Chapter2.sesi_2;

public interface Registration {
    void register(String name);
    Person getRegisteredPerson(String name);
}
/*
? Interface
* beda sama abstract class, interface cuma berisi abstract method dan nggak punya field sama sekali
* selain itu, interface nggak punya constructor dan nggak bisa di-instance
* interface harus diimplement suatu class menggunakan keyword """implements"""
* selain implementasi, sebuah interface juga mewariskan abstract method yang dimiliki interface lain dengan keyword """extends""" tanpa harus menulis implementasinya
? Inheritance
* mewariskan field dan method pada class turunan, tujuan buat reusability
* Superclass adalah class lama atau class yang mewariskan fitur, atau bisa dikatakan sebagai class induk
* Subclass merupakan class turunan atau yang mewarisi isi dai superclass atau sebagai class anak
* kaitan dengan abstraction dengan inheritance. abstract class nggak bisa diinstance kecuali terdapat subclass dari abstract class tersebut.
* penulisan implementasi dari sebuah abstract method dilakukan menggunakan overriding, yaitu method yang namnaya sama, return sama, dan parameter yang sama
* Tapi, implementasi dari method tersebut bisa beda-beda. Method yang dibuat implementasinya harus pakai annotation @Overriding.
* subclass yang berupa non-abstract class meng-extend sebuah abstract class, berarti semua abstract method di abstract class harus ditulis implementasinya.
* dengan kata lain. abstract method di class abstract class di-override sama subclass yang merupakan non-abstract class


* */