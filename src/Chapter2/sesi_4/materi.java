package Chapter2.sesi_4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class materi {
    public static void main(String[] args) {
//        try{
//            int data = 50/0;
//            System.out.println(data);
//        }catch(ArithmeticException e){
//            System.out.println(e.getMessage());
//        }

        Scanner scanner = null;
        try{
           scanner = new Scanner(new File("src/Chapter2/sesi_4/test.txt"));
           while (scanner.hasNext()){
               System.out.println(scanner.nextLine());
           }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }finally {
            if(scanner != null){
                scanner.close();
            }
        }

        //try with resource
        try(Scanner scanners = new Scanner(new File("src/Chapter2/sesi_4/test2.txt"))){
            while (scanners.hasNext()){
                System.out.println(scanners.nextLine());
            }
        }catch (FileNotFoundException e){
//            e.printStackTrace();
            System.out.println(e.getMessage());
        }

        // try with resources
        try(
                Scanner scanner2 = new Scanner(new File("src/Chapter2/sesi_4/testRead.txt"));
                PrintWriter writer = new PrintWriter(new File("src/Chapter2/sesi_4/testWrite.txt"))
        ){
            while (scanner2.hasNext()){
                writer.print(scanner2.nextLine());
                writer.println();
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }
}
/*
* Throw => keyword yang dipakai untuk membentuk object yang merupakan exception untuk membuat suatu validasi
* validasi ini biasanya dipakai dalam mengatur suatu business flow supaya program berjalan dengan sesuai
* ada 2 keyword yang berkaitan dengan throw yaitu throw dan throws
*
* throws cara menggunakan keyword throws ini adalha dengan menulisakn throws dan jenis exception-nya di sebelah parameter method pada sebuah method dideklarasikan
*
* */
