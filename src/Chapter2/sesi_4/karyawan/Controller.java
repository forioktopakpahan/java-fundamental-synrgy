package Chapter2.sesi_4.karyawan;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static Chapter2.sesi_3.karyawan.View.mainMenu;

public class Controller {
    public static Map<Integer, Karyawan> listKaryawan = new HashMap<>();

    public static void insertKaryawan(){
        LocalDate DoB = LocalDate.parse("2000-01-01");
        listKaryawan.put(1, new Karyawan(1, "Okto", DoB, "Pangaribuan", true));
        listKaryawan.put(2, new Karyawan(2, "Pakpahan", DoB, "Selatan", true));
        listKaryawan.put(3, new Karyawan(3, "Jhon", DoB, "TimorTimur", true));
    }
    // ! startView FUNCTION
    public static void startView(){
        mainMenu();
        int input;
        while (true){
            input = inputInt();
            if(input >= 1 && input <=4){
                break;
            }else{
                System.out.println("option not available");
            }
        }
        switch (input){
            case 1:
                addView();
                break;
            case 2:
                deleteView();
                break;
            case 3:
                editView();
                break;
            case 4:
                viewList();
                break;
        }
    }
    // ! addView FUNCTION
    public static void addView(){
        System.out.print("Masukkan nama : ");
        String nama = inputString();

        System.out.print("Tanggal Lahir(yyyy-mm-dd): ");
        String tanggal = inputString();

        System.out.print("Masukkan alamat : ");
        String alamat = inputString();

        System.out.print("Masukkan status (1/0) : ");
        int status = inputInt();

//?     public Karyawan(Integer id, String nama, LocalDate DoB, String alamat, Boolean status){
        int result = add(nama, tanggal, alamat, status);

        if(result == 1){
            System.out.println("input karyawan data success");
        }else {
            System.out.println("invalid to add karyawan data");
        }

        pressAnyKey();
        startView();
    }

    //! deleteView FUNCTION
    public static void deleteView(){
        listKaryawan();
        System.out.print("Silahkan pilih id ");
        int id = inputInt();
        int result = delete(id);
        if(result == 1){
            System.out.println("success to delete karyawan data");
        }else {
            System.out.println("invalid to input karyawan data");
        }
        pressAnyKey();
        startView();
    }

    //! editView FUNCTION
    public static void editView(){
        listKaryawan();
        System.out.print("Silahkan pilih id ");
        int id = inputInt();

        System.out.print("Masukkan nama : ");
        String nama = inputString();

        System.out.print("Tanggal Lahir(yyyy-mm-dd): ");
        String tanggal = inputString();

        System.out.print("Masukkan alamat : ");
        String alamat = inputString();

        System.out.print("Masukkan status (1/0) : ");
        int status = inputInt();

        int result = edit(id, nama, tanggal, alamat, status);
        if(result == 1){
            System.out.println("succes to edit karyawan data");
        }else{
            System.out.println("invalid to edit karyawan data");
        }
        pressAnyKey();
        startView();
    }

    //! viewList FUNCTION
    public static void viewList(){
        listKaryawan();
//        System.out.println("===============");
//        System.out.println("LIST KARYAWAN");
//        System.out.println("===============");
//        for(Map.Entry<Integer, Karyawan> k : listKaryawan.entrySet()){
//            View.karyawanData(k.getValue());
//        }
        pressAnyKey();
        startView();
    }

    // ! inputInt FUNCTION
    public static int inputInt(){
        int inputChoice;
        while (true){
            try{
                System.out.print("=> ");
                Scanner input = new Scanner(System.in);
                inputChoice = input.nextInt();
                break;
            }catch (Exception e){
                System.out.println("invalid choice");
            }
        }
        return inputChoice;
    }

    // ! inputString FUNCTION
    public static String inputString(){
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    // ! add FUNCTION
    public static int add(String nama, String tanggal, String alamat, int statusInt){
        try{
            int id = Collections.max(listKaryawan.keySet()) + 1;
            LocalDate DoB = LocalDate.parse(tanggal);
            if(!nama.matches("[A-Za-z]+")){
                throw new KaryawanException("Bagian nama invalid");
            }

            if(alamat == null || alamat.isEmpty()){
                throw new KaryawanException("Alamat kosong");
            }

            boolean status;
            if(statusInt == 1){
                status = true;
            }else if(statusInt == 0){
                status = false;
            }else {
                return 0;
            }
            //?     public Karyawan(Integer id, String nama, LocalDate DoB, String alamat, Boolean status){
            listKaryawan.put(id, new Karyawan(id, nama, DoB, alamat, status));
            return 1;
        }catch (KaryawanException e){
            System.out.println("error : " + e.getMessage());
            return 0;
        }catch (DateTimeException e){
            System.out.println("error : masukkan format yg benar " + e.getMessage());
            return 0;
        }catch (Exception e){
            System.out.println(e.getMessage());
            return 0;
        }
    }
    // ! delete FUNCTION
    public static int delete(int id){
        try{
            if(listKaryawan.remove(id) != null){
                return 1;
            }else {
                throw new KaryawanException("Data karyawan tidak ditemukan");
            }
        }catch (KaryawanException e){
            System.out.println("error : " + e.getMessage());
            return 0;
        }catch (Exception e){
            System.out.println("Unhandle error : " + e.getMessage());
            return 0;
        }

    }

    // ! edit FUNCTION
    public static int edit(int id, String nama, String tanggal, String alamat, int intStatus){
        try{
            if(!listKaryawan.containsKey(id)){
                throw new KaryawanRuntimeException("id tidak ditemukan");
            }
            LocalDate DoB = LocalDate.parse(tanggal);

            boolean status;
            if(intStatus == 1){
                status = true;
            }else if(intStatus == 0){
                status = false;
            }else {
                return 0;
            }
            listKaryawan.put(id, new Karyawan(id, nama, DoB, alamat, status));
            return 1;

        }catch (KaryawanRuntimeException e){
            System.out.println("error : " + e.getMessage());
            return 0;
        }
        catch (Exception e){
            return 0;
        }
    }

    // ! listKaryawan FUNCTION
    public static void listKaryawan(){
        System.out.printf("%-2s | %-10s | %-10s | %-15s | %-5s", "ID", "NAMA", "DoB", "ALAMAT", "STATUS");
        System.out.println();
        for (Integer i : listKaryawan.keySet()) {
            System.out.printf("%-2d | %-10s | %-10s | %-15s | %-5s",
                    listKaryawan.get(i).getId(),
                    listKaryawan.get(i).getNama(),
                    listKaryawan.get(i).getDoB(),
                    listKaryawan.get(i).getAlamat(),
                    listKaryawan.get(i).getStatus());
            System.out.println();
        }
    }

    // ! pressAnyKey FUNCTION
    public static void pressAnyKey(){
        System.out.println("Press any key to continue ....");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }
}
