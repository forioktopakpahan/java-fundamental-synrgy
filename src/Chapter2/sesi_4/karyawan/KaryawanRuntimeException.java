package Chapter2.sesi_4.karyawan;

public class KaryawanRuntimeException extends RuntimeException{
    public KaryawanRuntimeException(String message){
        super(message);
    }
}
