package Chapter2.sesi_4.karyawan;

public class View {
    public static void mainMenu(){
        System.out.println("============================");
        System.out.println("APLIKASI MANAJEMEN KARYAWAN");
        System.out.println("============================");
        System.out.println("1. ADD");
        System.out.println("2. DELETE");
        System.out.println("3. EDIT");
        System.out.println("4. VIEW LIST");
        System.out.println("5. EXIT");
    }

    public static void karyawanData(Karyawan karyawan){
        System.out.println("Id : " + karyawan.getId());
        System.out.println("Nama : " + karyawan.getNama());
        System.out.println("Tanggal Lahir : " + karyawan.getDoB());
        System.out.println("Alamat : " + karyawan.getAlamat());
        System.out.println("Statust Aktif : " + karyawan.getStatus());
        System.out.println();
    }
}
