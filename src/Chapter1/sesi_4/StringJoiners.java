package Chapter1.sesi_4;
import java.util.StringJoiner;

public class StringJoiners {
    public static void main(String[] args) {
        String[] rbgList = new String[3];
        rbgList[0] = "red";
        rbgList[1] = "green";
        rbgList[2] = "blue";
        StringJoiner rbgJoiner = new StringJoiner(",");
        for(int i = 0; i< rbgList.length; i++){
            rbgJoiner.add(rbgList[i]);
        }
        System.out.println(rbgJoiner);
    }
}
