package Chapter1.sesi_3;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Kalkulator {
        // menerima input terminal
        static Scanner input = new Scanner(System.in);
        static char operator;
        static double hasil = 0.0;
    public static void main(String[] args) {
        // ? membaca file lama
        List<String> history = new ArrayList<>();

        // ? container history untuk di push ke file lama
        List<String> new_history = new ArrayList<>();

        try(BufferedWriter history_writer = new BufferedWriter(new FileWriter("src/Chapter1.sesi_3/history.txt", true))){
//            FileWriter history_file = new FileWriter("src/Chapter1.sesi_3/history.txt");
//            BufferedWriter writer = new BufferedWriter(history_file);
            history = BacaRiwayatDariFile("src/Chapter1.sesi_3/history.txt");
            do{
                System.out.println("Pilih operasi:");
                System.out.println("1. Penambahan (+)");
                System.out.println("2. Pengurangan (-)");
                System.out.println("3. Perkalian (*)");
                System.out.println("4. Pembagian (/)");
                System.out.println("5. Cetak Riwayat ke File");
                System.out.println("6. Tampilkan Riwayat Terbaru di Terminal");
                System.out.println("7. Tampilkan Seluruh Riwayat di Terminal");
                System.out.println("8. Lihat Total Hasil Operasi");
                System.out.println("9. Keluar");
                System.out.println("10. Print to csv");
                System.out.print("Masukkan nomor operasi (1/2/3/4/5/6/7/8/9): ");

                int pilihan = input.nextInt();

                if(pilihan == 9){
                    break;
                }
                if(pilihan < 1 || pilihan > 10){
                    System.out.println("Pilihan tidak ada di menu!");
                    continue;
                }
                if(pilihan == 5){
                    CetakRiwayatKeFile(history_writer, new_history);
                    continue;
                }
                if(pilihan == 6){
                    TampilkanRiwayat(new_history);
                    continue;
                }
                if(pilihan == 7){
                    TampilkanRiwayat(history);
                    continue;
                }
                if(pilihan == 8){
                    HitungTotal(history);
                    continue;
                }
                if(pilihan == 10){
                    printToCSV(new_history);
                    continue;
                }
                Aritmatika(pilihan, new_history);

            }while (true);
        }catch (IOException e){
            e.printStackTrace();
            System.out.println("Gagal Menulis ke file history.txt");
        }
    }

    private static void printToCSV(List<String> histories) {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter("src/Chapter1.sesi_3/history.csv"))){
            for(String history : histories){
                writer.write(history);
                writer.newLine();
            }
            System.out.println("end write");
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    // ! Aritmatika method
    public static void Aritmatika(int pilihan, List<String> history){
        System.out.print("Masukkan angka pertama: ");
        double int1 = input.nextDouble();
        System.out.print("Masukkan angka kedua: ");
        double int2 = input.nextDouble();

        switch (pilihan){
            case 1:
                operator = '+';
                hasil = int1 + int2;
                break;
            case 2:
                operator = '-';
                hasil = int1 - int2;
                break;
            case 3:
                operator = '*';
                hasil = int1 * int2;
                break;
            case 4:
                operator = '/';
                hasil = int1 / int2;
                break;
            default:
                operator = ' ';
                System.out.println("Operasi Tidak Valid");
        }

        String operasi = int1 + " " + operator + " " + int2 + " = " + hasil;

        history.add(operasi);

        System.out.println(operasi);
    }

    // ! CetakRiwayatKeFile method
    public static void CetakRiwayatKeFile(BufferedWriter History_writer, List<String> histories){
        try{
            System.out.println("Menyimpan riwayat ke history");
            for(String history:histories){
                History_writer.write(history + "\n");
            }
            System.out.println("Riwayat telah disimpan");
        }catch (IOException e){
            System.out.println("Gagal mencetak");
            e.printStackTrace();
        }
    }

    // ! TampilkanRiwayat method
    public static void TampilkanRiwayat(List<String> histories){
        for(String history : histories){
            System.out.println(history);
        }
    }

    // ! HitungTotal method
    public static void HitungTotal(List<String> histories){
        double total = 0.0;
        for(String history:histories){
            // regex produces array
            String[] cont = history.split("=");
            double num = Double.parseDouble(cont[1]);
            total += num;
        }
        System.out.println("Total hasil semua operasi: " + total);
    }

    // ! Baca Riwayat dari File
    public static List<String> BacaRiwayatDariFile(String filename){
        List<String> history = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new FileReader(filename))){
            String line;
            while((line = reader.readLine()) != null){
                history.add(line);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return history;

    }
}
/*
! Issue
* Kenapa ketika saya menginstantiate BufferedWriter class (history) di dalam block try tidak bisa melakukan penulisan kedalam file history? Tetapi ketika saya menginstantiate BufferedWriter di parameter try bisa melakukan penulisan
* */