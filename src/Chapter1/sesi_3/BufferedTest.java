package Chapter1.sesi_3;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedTest {
    public static void main(String[] args) {
        FileWriter geek_file;
        try{
            //create file in specific folder
            geek_file = new FileWriter("src/Chapter1.sesi_3/test1.txt");
            BufferedWriter geekWrite = new BufferedWriter(geek_file);
            System.out.println("Buffered writer start writing");

            geekWrite.write("start writing in string");
            geekWrite.newLine();
            geekWrite.write('s'); //char
            geekWrite.newLine();
            geekWrite.write(69); //char
            geekWrite.newLine();
            geekWrite.write(49);
            geekWrite.newLine();
            geekWrite.write(50);
            geekWrite.flush();
            geekWrite.close();

            System.out.println("Writen successfully");

        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
